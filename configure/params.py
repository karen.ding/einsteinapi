# coding=utf-8
class Params:

    def priority(self, p):
        priority = {"p0": 0, "p1": 1, "p2": 2, "p3": 3}
        return priority.get(p)

    def excute_type(self, type):
        excute_type = {"manual": 0, "automated": 1, "automatable": 2, "not automatable": 3, "ready for automation": 4}
        return excute_type.get(type)


# pa = Params()
# print(pa.excute_type("manual"))
# import os
# print(os.path.dirname(os.path.abspath(__file__)))
