import os


class GetData:

    def temp(self, a, b, l):
        l.insert(0, b)
        l.insert(0, a)
        t = tuple(l)
        return t

    def get_preconditions(self, account, extension, entries):
        length = len(entries)
        str1 = "<li>%s</li>\n"
        str2 = ""
        for i in range(length):
            str2 = str2 + str1

        data = "<html>\n<head>\n<style type=\"text/css\">\nh1 {font-size:12px;}\nh1 {color:#669966}\nh1 {font-family: Arial;}\nh1 {font-weight:bold;}\np {font-size:12px;}\np {font-family: Arial;}\nli{font-size:12px;}\n</style>\n</head>\n<body>\n<h1>Account type(/s): </h1>\n<p>%s</p>\n<h1>Extension type(/s): </h1>\n<p>%s</p>\n<h1>Entry point(/s): </h1>\n" + str2 + "</body>\n</html>\n"

        return data % self.temp(account, extension, entries)

    def get_parameters_from_jekins(self, parameter_name):
        return os.getenv(parameter_name)


# d = GetData()
# data = d.get_preconditions("karen.ding", "extension", ["aaa", "bbbb", "ccccc"])
# print(data)

