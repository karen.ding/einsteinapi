import json


class OperateJson:
    def __init__(self, path=None):
        if path:
            self.path = path
        else:
            self.path = r"../configure/cookie.json"
        # self.data = self.read_data()
        # print(self.data)

    def write_json(self, data):
        """
        write cookies
        :param data: {'jwt': 'eyJhbGciOiJIUzI1...'}
        """
        with open(self.path, 'w') as fp:
            fp.write(json.dumps(data))

    def read_data(self):
        """
        read cookies
        :return: {'jwt': 'eyJhbGciOiJIUzI1...'}
        """
        with open(self.path) as fp:
            data = json.load(fp)
            return data

    def get_custom_value(self, labels):
        """
        :param labels: ["Functionality","Glip Android"]
        :return: [{'value': 11813}, {'value': 46605}]
        """
        id = []
        data = self.read_data()
        length = len(data["keywords"])
        # print(data["keywords"])
        for label in labels:
            for i in range(length):
                if label == data["keywords"][i]["value"]:
                    label_input = {"value": data["keywords"][i]["id"]}
                    id.append(label_input)
        return id




# oj = OperateJson(r"../configure/custom_values.json")
# print(oj.get_custom_value(["Automated_iOS", "Native automated_iOS", "Automated_Android", "Native automated_Android"]))
# oj = OperateJson(r"../configure/assignee.json")
# print(oj.read_data())
