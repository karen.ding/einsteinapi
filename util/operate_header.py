import requests
import json
from util.operate_json import OperateJson


class OperateHeader:

    # 获取登陆cookie
    def get_cookies(self, url, data):
        """
        get cookies return from login request
        :param url: "https://einstein.ringcentral.com/api/login"
        :param data: {"username":username,"password":pw}
        :return:<RequestsCookieJar[<Cookie jwt=eyJhbGci...>]>
        """
        data = json.dumps(data)
        cookie_jar = requests.post(url, data).cookies
        return cookie_jar

    # 将cookie写入json文件
    def write_cookies(self, url, data):
        """
        write cookies into json file
        :param url: "https://einstein.ringcentral.com/api/login"
        :param data: {"username":username,"password":pw}
        :return: {'jwt': 'eyJhbGciO..'}
        """
        cookie_jar = self.get_cookies(url, data)
        # print(cookie_jar)
        # change into dict
        cookies = requests.utils.dict_from_cookiejar(cookie_jar)
        op_json = OperateJson()
        op_json.write_json(cookies)
        return cookies



# op_h = OperateHeader()
# url = "https://einstein.ringcentral.com/api/login"
# data = {"username": "karen.ding", "password": "Pwd.123456"}
# print(op_h.write_cookies(url, data))



