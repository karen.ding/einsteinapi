# coding=utf-8

import requests
import json
from util.operate_header import OperateHeader
from util.operate_json import OperateJson
from configure.params import Params
from util.get_data import GetData


class EinsteinApi:
    def __init__(self, username, pw):
        self.login(username, pw)
        self.data = OperateJson()
        self.header = self.data.read_data()
        self.params = Params()
        # self.project_id = project_id
        self.get_list_of_keywords()
        self.get_data = GetData()

    def login(self, username, pw):
        """
        login and get cookies
        :param username:
        :param pw:
        """
        op_h = OperateHeader()
        url = "https://einstein.ringcentral.com/api/login"
        data = {"username": username, "password": pw}
        op_h.write_cookies(url, data)

    def get_suit(self, projectId):
        data = {"id": projectId, "withChildren": "true"}
        res = requests.get(url="https://einstein.ringcentral.com/expand/project", params=data, cookies=self.header).json()
        return res

    def get_project_info(self,projectId):
        data = {"id": projectId}
        res = requests.get(url="https://einstein.ringcentral.com/info/project", params=data,cookies=self.header).json()
        return res

    def create_suit(self, name, parent_id=None):
        """
        :param name: suit name
        :param parent_id: parent suit id/project id
        :return:
        """
        if parent_id:
            data = {"name": name, "parentId": parent_id}
        else:
            data = {"name": name, "projectId": 1312}
        res = requests.post(url="https://einstein.ringcentral.com/create/suite", params=data, cookies=self.header).json()
        return str(res["item"]["id"])

    def get_test_case_info(self, caseId):
        data = {"id": caseId}
        res = requests.get(url="https://einstein.ringcentral.com/info/case", params=data, cookies=self.header).json()
        return res

    def create_test_case(self, parent_id, name, summary):
        """
        :param parent_id: parent suit id
        :param name: case name
        :param summary: summary
        :return:
        """
        data = {"parentId": parent_id, "name": name, "summary": summary}
        res = requests.post(url="https://einstein.ringcentral.com/create/case", params=data, cookies=self.header).json()
        return str(res["item"]["id"])

    def get_test_case_version(self, case_id):
        """
        :param case_id: "1234"(str)
        :return: versionid(int)
        """
        value = str([1, 2, case_id])
        data = {"ids": value}
        res = requests.post(url="https://einstein.ringcentral.com/info/versions", params=data, cookies=self.header).json()
        # return str(res["versionsMap"][case_id][0]["id"])
        return res

    def update_test_case(self, id, version_id, account, extension, entries):
        preconditions = self.get_data.get_preconditions(account, extension, entries)
        data = {"id": id, "versionId": version_id, "preconditions": preconditions}
        print(data)
        res = requests.post(url="https://einstein.ringcentral.com/update/case", params=data, cookies=self.header).json()
        return res


    def get_list_of_keywords(self):
        op_j = OperateJson(r"../configure/custom_values.json")
        data = {"projectId": "1312"}
        res = requests.post(url="https://einstein.ringcentral.com/info/projectKeywords", params=data, cookies=self.header).json()
        op_j.write_json(res)
        return res

    def update_test_case_custom_values(self, case_id, priority, labels, execution_type=None):
        """
        :param case_id:
        :param priority: "p0"/"p1"/"p2"/"p3"(str)
        :param labels: ["Functionality"](list)
        :param execution_type: "automated"(str)
        :return:
        """
        op_j = OperateJson(r"../configure/custom_values.json")
        priority = self.params.priority(priority)
        values = op_j.get_custom_value(labels)
        values_str = json.dumps(values)
        if execution_type:
            execution_type = self.params.excute_type(execution_type)
        else:
            execution_type = "manual"
        data = {"id": case_id, "priority": priority, "executionType": execution_type, "values": values_str}
        # print(data)
        res = requests.post(url="https://einstein.ringcentral.com/update/testCaseCustomValues", params=data, cookies=self.header).json()
        return res


    def get_projectList(self, data=None):
        res = requests.get(url="https://einstein.ringcentral.com/info/projects", params=data, cookies=self.header).json()
        return res

    def get_testplan(self, project):
        data = {"projectId": project}
        res = requests.get(url="https://einstein.ringcentral.com/info/testplans", params=data, cookies=self.header).json()
        return res

    def get_build(self, testplan):
        data = {"testPlanId": testplan}
        res = requests.get(url="https://einstein.ringcentral.com/testPlans/builds", params=data, cookies=self.header).json()
        return res

    def get_testplan_case(self, testplanId):
        data = {"id": testplanId, "type": "testplan", "withObsolete": "false", "withAssignees": "true"}
        res = requests.post(url="https://einstein.ringcentral.com/expandTestPlans/index", params=data, cookies=self.header).json()
        return res

    def assign_case(self, testplanId, assign):
        """
        :param testplanId:
        :param assign: [{"versionId":"34391","userId":7089178},{"versionId":"34498","userId":7089178}]
        :return:
        """
        data = {"testPlanId": testplanId, "executionParameters": [], "assignees": assign}
        res = requests.post(url="https://einstein.ringcentral.com/update/testCaseVersionsAssignee", params=data, cookies=self.header).json()
        return res

    def assign_test_case1(self, testplan_id, assign_list):
        data = {"testPlanId": testplan_id, "executionParameters": [],"assignees": assign_list}
        res = requests.post(url="https://einstein.ringcentral.com/update/testCaseVersionsAssignee", params=data,cookies=self.header).json()
        return res



# suit_id = R.create_suit("test_karen")
# case_id = R.create_test_case(suit_id, "testapi",  "api test")
# case_ver = R.get_test_case_version("213749")
# print(case_ver)
# R.update_test_case("213956", "482168", "karen.ding", "test", ["aaaaaaaa", "bbbbbbb", "cccccc", "dddddd"])
# R.update_test_case_custom_values(case_id, "p3", ["Functionality", "Glip Android"])

# print(R.get_projectList())
# print(R.create_suit("testapi"))
# print(R.create_test_case("213496", "testapi-karen",  "api test"))
# print(R.update_test_case_custom_values("211659", "p0", ["Functionality"], "automated"))
# print(R.update_test_case("211659", versionId="477606", name='test4'))
# print(R.get_test_case_version("135660"))
# print(R.update_test_case("213470","481338","ss"))
# print(R.get_testplan("1312"))
# # print(R.get_build("8556514"))
# print(R.get_testplan_case("270097"))
# ID = "270097"
# case = [{'versionId': 314201, 'userId': 1207}, {'versionId': 24800, 'userId': 1207}, {'versionId': 312332, 'userId': 1207}, {'versionId': 24061, 'userId': 1207}, {'versionId': 23746, 'userId': 1207}, {'versionId': 28613, 'userId': 1207}, {'versionId': 33546, 'userId': 1207}, {'versionId': 329375, 'userId': 1207}, {'versionId': 394597, 'userId': 1207}, {'versionId': 33798, 'userId': 1207}, {'versionId': 83093, 'userId': 1207}, {'versionId': 24048, 'userId': 1207}, {'versionId': 24004, 'userId': 1207}, {'versionId': 321135, 'userId': 1207}, {'versionId': 324716, 'userId': 1207}, {'versionId': 329313, 'userId': 1207}, {'versionId': 28326, 'userId': 1207}, {'versionId': 23917, 'userId': 1207}, {'versionId': 23907, 'userId': 1207}, {'versionId': 329566, 'userId': 1207}, {'versionId': 325246, 'userId': 1207}, {'versionId': 325244, 'userId': 6697439}, {'versionId': 325248, 'userId': 6697439}, {'versionId': 333792, 'userId': 6697439}, {'versionId': 85113, 'userId': 6697439}, {'versionId': 91790, 'userId': 6697439}]
# case =json.dumps(case)
# print(type(case))
# print(R.assign_case(ID,case))
# print(type(case))
# print(R.assign_case(ID, case))
# print(R.get_test_case_info({"135660"}))
# print(R.get_suit("1312"))
# print(R.get_project_info("1312"))
# print(R.assign_test_case1(ID,case))
# print(R.get_test_case_info(220412))




